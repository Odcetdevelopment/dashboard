import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppComponent } from './app.component';
// Import containers


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainLayoutModule } from './containers/main-layout/main-layout.module';
import { AppRoutingModule } from './app.routing';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    
    BrowserModule,
    BrowserAnimationsModule,
    MainLayoutModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
   
  ],
  exports: [RouterModule],
  // providers: [{
  //   provide: LocationStrategy,
  //   useClass: PathLocationStrategy
  // }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
