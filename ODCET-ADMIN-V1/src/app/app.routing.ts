import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './containers/main-layout/main-layout.component';



export const routes: Routes = [
  { path: '',  loadChildren: './containers/main-layout/main-layout.module#MainLayoutModule'},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
