interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-home',
    children: [
    ]
    // badge: {
    //   variant: 'info',
    //   text: 'NEW'
    // }
  },
  // {
  //   title: true,
  //   name: 'Theme'
  // },
  {
    name: 'Trending',
    url: '/theme/colors',
    icon: 'icon-fire',
    children: [
      {
        name: 'Overview',
        url: '/base/cards',
        icon: 'icon-eye'
      },
      {
        name: 'Userwise List',
        url: '/base/carousels',
        icon: 'icon-list'
      },
      {
        name: 'Area WideList',
        url: '/base/collapses',
        icon: 'icon-list'
      },]
  },
  {
    name: 'Manage Users',
    url: '/theme/typography',
    icon: 'icon-people',
    children: [
    ]
  },
  // {
  //   title: true,
  //   name: 'Components'
  // },
  {
    name: 'Manage Videos',
    url: '/base',
    icon: 'icon-playlist',
    children: [
         ]
  },
  {
    name: 'Users Count',
    url: '/buttons',
    icon: 'icon-badge',
    children: [
     
    ]
  },
  {
    name: 'Logout',
    url: '/charts',
    icon: 'icon-logout',
    children: [
    ]
  },
 
 
  
  
];
